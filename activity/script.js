
console.log("Hello World")


fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(data => {console.log(data)})



fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		description: 'Hello World',
		status: true,
		dateCompleted: '',
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		description: 'Hello World',
		status: true,
		dateCompleted: '',
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated post"
	})
})
.then(res => res.json())
.then(data => console.log(data))



fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))





